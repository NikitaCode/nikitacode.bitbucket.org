// Подключаем модуль и ставим на прослушивание 8080-порта - 80й обычно занят под http-сервер
var io = require('socket.io').listen(8080); 
// Отключаем вывод полного лога - пригодится в production'е
//io.set('log level', 1);
var entinity = require('./entinity.js'); 
// Навешиваем обработчик на подключение нового клиента

var players=[{x:1,y:2,id:0,name:'AI',ping:0}];
players=[];
console.log('--- server start ---');  
/*
using basic multiplayer model:
server: send anything
server: process any gameplay
client: send to server controlls
client: output game things 

ups: 10
*/


//var main_timer;

//start output cycle 

function randomInteger(min, max) 
{
    var rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
}


var ClassicalNoise = function(r) { // Classic Perlin noise in 3D, for comparison 
	if (r == undefined) r = Math;
  this.grad3 = [[1,1,0],[-1,1,0],[1,-1,0],[-1,-1,0], 
                                 [1,0,1],[-1,0,1],[1,0,-1],[-1,0,-1], 
                                 [0,1,1],[0,-1,1],[0,1,-1],[0,-1,-1]]; 
  this.p = [];
  for (var i=0; i<256; i++) {
	  this.p[i] = Math.floor(r.random()*256);
  }
  // To remove the need for index wrapping, double the permutation table length 
  this.perm = []; 
  for(var i=0; i<512; i++) {
		this.perm[i]=this.p[i & 255];
  }
};

ClassicalNoise.prototype.dot = function(g, x, y, z) { 
    return g[0]*x + g[1]*y + g[2]*z; 
};

ClassicalNoise.prototype.mix = function(a, b, t) { 
    return (1.0-t)*a + t*b; 
};

ClassicalNoise.prototype.fade = function(t) { 
    return t*t*t*(t*(t*6.0-15.0)+10.0); 
};

  // Classic Perlin noise, 3D version 
ClassicalNoise.prototype.noise = function(x, y, z) { 
  // Find unit grid cell containing point 
  var X = Math.floor(x); 
  var Y = Math.floor(y); 
  var Z = Math.floor(z); 
  
  // Get relative xyz coordinates of point within that cell 
  x = x - X; 
  y = y - Y; 
  z = z - Z; 
  
  // Wrap the integer cells at 255 (smaller integer period can be introduced here) 
  X = X & 255; 
  Y = Y & 255; 
  Z = Z & 255;
  
  // Calculate a set of eight hashed gradient indices 
  var gi000 = this.perm[X+this.perm[Y+this.perm[Z]]] % 12; 
  var gi001 = this.perm[X+this.perm[Y+this.perm[Z+1]]] % 12; 
  var gi010 = this.perm[X+this.perm[Y+1+this.perm[Z]]] % 12; 
  var gi011 = this.perm[X+this.perm[Y+1+this.perm[Z+1]]] % 12; 
  var gi100 = this.perm[X+1+this.perm[Y+this.perm[Z]]] % 12; 
  var gi101 = this.perm[X+1+this.perm[Y+this.perm[Z+1]]] % 12; 
  var gi110 = this.perm[X+1+this.perm[Y+1+this.perm[Z]]] % 12; 
  var gi111 = this.perm[X+1+this.perm[Y+1+this.perm[Z+1]]] % 12; 
  
  // The gradients of each corner are now: 
  // g000 = grad3[gi000]; 
  // g001 = grad3[gi001]; 
  // g010 = grad3[gi010]; 
  // g011 = grad3[gi011]; 
  // g100 = grad3[gi100]; 
  // g101 = grad3[gi101]; 
  // g110 = grad3[gi110]; 
  // g111 = grad3[gi111]; 
  // Calculate noise contributions from each of the eight corners 
  var n000= this.dot(this.grad3[gi000], x, y, z); 
  var n100= this.dot(this.grad3[gi100], x-1, y, z); 
  var n010= this.dot(this.grad3[gi010], x, y-1, z); 
  var n110= this.dot(this.grad3[gi110], x-1, y-1, z); 
  var n001= this.dot(this.grad3[gi001], x, y, z-1); 
  var n101= this.dot(this.grad3[gi101], x-1, y, z-1); 
  var n011= this.dot(this.grad3[gi011], x, y-1, z-1); 
  var n111= this.dot(this.grad3[gi111], x-1, y-1, z-1); 
  // Compute the fade curve value for each of x, y, z 
  var u = this.fade(x); 
  var v = this.fade(y); 
  var w = this.fade(z); 
   // Interpolate along x the contributions from each of the corners 
  var nx00 = this.mix(n000, n100, u); 
  var nx01 = this.mix(n001, n101, u); 
  var nx10 = this.mix(n010, n110, u); 
  var nx11 = this.mix(n011, n111, u); 
  // Interpolate the four results along y 
  var nxy0 = this.mix(nx00, nx10, v); 
  var nxy1 = this.mix(nx01, nx11, v); 
  // Interpolate the two last results along z 
  var nxyz = this.mix(nxy0, nxy1, w); 

  return nxyz; 
};

var no=new ClassicalNoise;



var map={t:0,x:0,y:0,b:[]}

//block_textures=new Image(); 

function init_map(_x,_y)
{
	map.chunk=[];
	map.b=[];
	for(let i=0;i<_x;i++)
	{
		map.b[i]=[];
		for(let r=0;r<_y;r++)
		{
			map.b[i][r]={t:-1};
		}
	}
	for(let i=0;i<_x/4;i++)
	{
		map.chunk[i]=[];
		for(let r=0;r<_y/4;r++)
		{
			map.chunk[i][r]={};
			map.chunk[i][r].pl_vis=0;
			map.chunk[i][r].mob_sp=0;
		}
	}
	map.x=_x;
	map.y=_y;
}

function gen_map()
{
	for(let i=0;i<map.x;i++)
	{
		map.b[i]=[];
		for(let r=0;r<map.y;r++)
		{
			map.b[i][r]={t:0};
			var o=Math.floor(no.noise(3,3,i/10)*7)+map.y/2;

			if(r==o)
				map.b[i][r].t=1;
			if(r>o)
				map.b[i][r].t=2;
		}
	}
}

function drawblock(_x,_y,_b)
{
	cx.drawImage(block_textures,0+_b.t*32,0,32,32,_x,_y,32,32);
}


function chunk_try_spawn(_x,_y,_t,is_gr)//chunk x,y
{
	let bx,by;

	bx=_x*4+randomInteger(0,3);
	by=_y*4+randomInteger(0,3);

	let inf=entinity.mobs_info[_t];

	for(let i=0;i<Math.floor(inf.sizex+1);i++)
	for(let r=0;r<Math.floor(inf.sizey+1);r++)
	{
		if(bx+i>=map.x || by+r>=map.y)
		{
			return;
		}
		if(map.b[bx+i][by+r].t!=0)
			return;

	}

	console.log("near spawned:"+bx+","+by);

	let gr=0;
	if(is_gr)
	{
		let r;
		for(r=0;r<4;r++)
		for(let i=0;i<Math.floor(inf.sizex+1);i++)
		{
			if(bx+i>=map.x || by+r+Math.floor(inf.sizey+1)>=map.y)
			{
				return;
			}
			if(map.b[bx+i][by+r+Math.floor(inf.sizey+1)].t!=0)
			{
				i=Math.floor(inf.sizex+1);
				r=4;
				gr=1;
			}
		}
		if(gr==0) 
			return;
		_x+=r;
	}
	console.log("spawned");

	entinity.add_obj(obj,{t:2,e_t:_t,x:bx+inf.sizex/2,y:by+inf.sizey/2,xx:0,yy:0});


}


function upd()
{
	ti++;

	for(let num in players)//mark player vision
	{
		let cx=Math.floor(players[num].x/4);
		let cy=Math.floor(players[num].y/4);


		for(let i=-4;i<=4;i++)
		for(let r=-4;r<=4;r++)
		{
			bx=cx+i;
			by=cy+r;
			if(map.chunk[bx])
			if(map.chunk[bx][by])
			if(map.chunk[bx][by].pl_vis!==null)
			{
				map.chunk[bx][by].pl_vis++;
			}
		}
		console.log("pl_vis:"+map.chunk[cx][cy].pl_vis);
	}

	let ray_sp=[];

	let pos_vecs;
	pos_vecs=[[1,0],[-1,0],[0,1],[0,-1]];

	for(let num in players)//mark over player vision
	{
		let cx=Math.floor(players[num].x/4);
		let cy=Math.floor(players[num].y/4);


		for(let r=0;r<4;r++)
		for(let i=-5;i<=5;i++)
		{
			bx=cx+5*pos_vecs[r][0]+i*pos_vecs[r][1];
			by=cy+5*pos_vecs[r][1]+i*pos_vecs[r][0];
			if(map.chunk[bx])
			if(map.chunk[bx][by])
			if(!map.chunk[bx][by].pl_vis)
			{
				if(!map.chunk[bx][by].mob_sp)
					ray_sp.push([bx,by]);
				map.chunk[bx][by].mob_sp=1;
			}
		}
	}


	entinity.upd(obj,map);

	//console.log(JSON.stringify(ray_sp));


	for(let num in players)//un mark player vision
	{
		let cx=Math.floor(players[num].x/4);
		let cy=Math.floor(players[num].y/4);


		for(let i=-5;i<=5;i++)
		for(let r=-5;r<=5;r++)
		{
			bx=cx+i;
			by=cy+r;
			if(map.chunk[bx])
			if(map.chunk[bx][by])
			{
				map.chunk[bx][by].pl_vis=0;
				map.chunk[bx][by].mob_sp=0;
			}
		}
	}


	for(let i in ray_sp)
	{
		if(Math.random()<0.004)
			chunk_try_spawn(ray_sp[i][0],ray_sp[i][1],2,1);
	}



}


init_map(800,800);
gen_map();
var ti=0;

function get_chunk(_x,_y)
{
	let res={x:_x,y:_y,b:[]};
	for(let i=0;i<4;i++)
	{
		res[i]=[];
		if(map.b[_x+i])
		for(let r=0;r<4;r++)
		if(map.b[_x+i][_y+r])
		{
			res[i][r]=map.b[_x+i][_y+r];		
		}
	}
	return res;
}

setInterval(function() 
{
	upd();

	if(ti%20==0)
	{

		//console.log('pl:'+JSON.stringify(players));  
		//console.log('obj:'+JSON.stringify(obj));  
	}
}, 30);  

var obj={};

//let o_id=entinity.add_obj(obj,{t:2,e_t:1,x:5+Math.random()*10,y:map.y/2-10,xx:0,yy:0,name:'guest',keys:{w:0,s:0,a:0,d:0}});

io.sockets.on('connection', function (socket) 
{

	console.log("...");
	// Basic player name
	let time = (new Date).toLocaleTimeString();
	let i;
	for(i=0;players[i]!=null;i++);

	players[i]={x:5+Math.random()*10,y:map.y/2-10,xx:0,yy:0,id_obj:-1,name:'guest',keys:{w:0,s:0,a:0,d:0}};
	let ID = i;

	let o_id=entinity.add_obj(obj,{t:1,x:5+Math.random()*10,y:map.y/2-10,xx:0,yy:0,name:'guest',keys:{w:0,s:0,a:0,d:0}});
	entinity.add_obj(obj,{t:2,e_t:1,x:5+Math.random()*10,y:map.y/2-5,xx:0,yy:0});
	players[ID].id_obj=o_id;

	console.log('--- Somebody connected to server --- hes got id:'+ID);  

	socket.json.send({'event': 'connected', 'id': ID , players:players, 'time': time,tick:ti});
	socket.broadcast.json.send({'event': 'userJoined', 'id': ID, 'time': time});

	

	socket.on('message', function (msg) {
		if(msg.event=='message')
		{
			let time = (new Date).toLocaleTimeString();
			// Уведомляем клиента, что его сообщение успешно дошло до сервера
			socket.json.send({'event': 'messageSent', 'id': ID, 'text': msg.text, 'time': time});
			// Отсылаем сообщение остальным участникам чата
			socket.broadcast.json.send({'event': 'messageReceived', 'id': ID, 'text': msg.text, 'time': time});
		}
		if(msg.event=='setpos')
		{
			players[ID].x=msg.pos.x;
			players[ID].yy=0;
			players[ID].xx=0;
			players[ID].y=msg.pos.y;
			socket.json.send({'event': 'messageSent', 'id': ID, 'text': 'pos changred', 'time': time});
		}
		if(msg.event=='player_info')
		{
			if(msg.player!=null)
				players[ID]=msg.player;
		}


		if(msg.event=='need_part')
		{
			socket.json.send({'event': 'map_part', part:get_chunk(msg.x,msg.y)});			
		}

		if(msg.event=='obj_info')
	{
			obj[players[ID].id_obj]=msg.obj;
			//alert(JSON.stringify(msg.obj));
		}
		players[ID].ping=0;
	});

	socket.on('disconnect', function() {
		let time = (new Date).toLocaleTimeString();
		io.sockets.json.send({'event': 'userSplit', 'id': ID, 'time': time});
		delete obj[players[ID].id_obj];
		delete players[ID];
	});

	var l_ti=0;

	setInterval(function() 
	{
		l_ti++;
		if(players[ID]==null)
			return;
		let pl=players[ID];
		socket.json.send({'event': 'players', 'text': players, 'tick': ti});
		if(ti%10==0)
		{
			//socket.json.send({'event': 'y_obj', 'id': players, 'tick': ti});
		}

		for(let key in obj)
		//if(ti%5==0)
		{
			let cob=obj[key];
			if(Math.abs(cob.x-pl.x)<20 && Math.abs(cob.y-pl.y)<20)
				socket.json.send({'event': 'obj_info', 'obj': cob, 'id': key,tick:ti});

		}

	}	
	, 30);  

});



/*

	setInterval(function() 
	{
		players[0].x+=Math.random()*10-Math.random()*10;
		players[0].y+=Math.random()*10-Math.random()*10;

		if(players[0].x< 10)players[0].x=10;
		if(players[0].x>100)players[0].x=100;
		if(players[0].y< 10)players[0].y=10;
		if(players[0].y>100)players[0].y=100;

		socket.broadcast.json.send({'event': 'players', 'text': players});
		//alert('Прошла 1 секунда');
	}
	, 100);  

*/
