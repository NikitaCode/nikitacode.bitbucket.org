function ab_str(buf) 
{
  return String.fromCharCode.apply(null, new Uint16Array(buf));
}

function str_ab(str) 
{
  var buf = new ArrayBuffer(str.length*2); // 2 bytes for each char
  var bufView = new Uint16Array(buf);
  for (var i=0, strLen=str.length; i<strLen; i++) 
  {
    bufView[i] = str.charCodeAt(i);
  }
  return buf;
}


try
{

	module.exports.ab_str=ab_str;
	module.exports.str_ab=str_ab;
}catch(e){}
