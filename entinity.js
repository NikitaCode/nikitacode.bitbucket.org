
//var obj={};
/*
t:
0-????
1-player
2-item
3-enemy
4-bullet
*/


function randomInteger(min, max) 
{
    var rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
}

function add_obj(obj,_nobj)
{
	let id=randomInteger(0,100000);
	while(obj[id])
	{
		id=randomInteger(0,100000);
		//used if random hit already used id
	}

	obj[id]=_nobj;

	if(obj[id].t==1)//player
	{
		obj[id].hp=100;
		
	}

	if(obj[id].t==2)//mob
	{
		obj[id].hp=mobs_info[obj[id].e_t].hp;     
		obj[id].keys={}; 
		obj[id].desp=400; 
	}

	return id;
}


function test_poss_mov(map,x,y,sx,sy,_side)
{

	//0-up
	//1-down
	//2-left
	//3-right

	let xsize=Math.floor(sx+2);
	let ysize=Math.floor(sy+2);

	if(_side==0 || _side==1)
	{
		for(let r=0;r<xsize;r++)
		{
			let tx=-sx/2+r*sx/(xsize-1);
			let ty=sy/2;
			if(_side==0)
				ty=-sy/2;

			var testx=x+tx;
			var testy=y+ty;

			let bx=Math.floor(testx);
			let by=Math.floor(testy);

			if(!map.b[bx] || !map.b[bx][by] || map.b[bx][by].t!=0)
				return false;

		}
	}

	if(_side==2 || _side==3)
	{

		for(let r=0;r<ysize;r++)
		{
			let tx=sx/2;
			if(_side==2)
				tx=-sx/2;
			let ty=-sy/2+r*sy/(ysize-1);

			var testx=x+tx;
			var testy=y+ty;

			let bx=Math.floor(testx);
			let by=Math.floor(testy);

			if(!map.b[bx] || !map.b[bx][by] || map.b[bx][by].t!=0)
				return false;

		}

	}
	return true;
}



var test;
function upd_single_obj(cob,map,_ticks)
{
	if(_ticks!=1)
		test="ok:"+_ticks;
	if(_ticks>10)
		return cob;

	if(cob.t==2)
	if(map.chunk)
	if(map.chunk[Math.floor(cob.x/4)] && map.chunk[Math.floor(cob.x/4)][Math.floor(cob.y/4)])
	if(map.chunk[Math.floor(cob.x/4)][Math.floor(cob.y/4)].pl_vis!=undefined)
	{
		if(map.chunk[Math.floor(cob.x/4)][Math.floor(cob.y/4)].pl_vis)
			cob.desp=400;
		cob.desp--;
		if(cob.desp<0)
			cob.t=0;
	}
	for(var tiii=0;tiii<_ticks;tiii++)
	{
		if(cob.t==0)
		{
			
		}
		if(cob.t==1)
		{

			cob.yy+=0.02;
			//cob.xx+=0.02;
			cob.xx*=0.7;

			
			var testp=[{x:-0.45,y:-0.45},{x:-0.45,y:0.45},{x:0.45,y:-0.45},{x:0.45,y:0.45}];


			if(cob.keys.a==1)
			{
				cob.xx+=-0.04;
			}
			if(cob.keys.d==1)
			{
				cob.xx+=0.04;
			}
			if(cob.keys.w==1)
			{

				for(let r=0;r<4;r++)
				{
					var ctp=testp[r];

					var testx=cob.x+ctp.x;
					var testy=cob.y+ctp.y+0.03;
					let bx=Math.floor(testx);
					let by=Math.floor(testy);
					if(map.b[bx])
					if(map.b[bx][by])
					{
						let bl=map.b[bx][by];
						if(bl.t!=0)
						{
							cob.yy=-0.3;
						}
					}
				}

			}


			if(cob.yy>0.5) cob.yy=0.5;
			if(cob.x< 0.5)cob.x=0.5;
			if(cob.x>map.x-0.5)cob.x=map.x-0.5;
			if(cob.y< 0.5)cob.y=0.5;
			if(cob.y>map.y-0.5)cob.y=map.y-0.5;

			for(let r=0;r<4;r++)
			{
				var ctp=testp[r];

				var testx=cob.x+ctp.x;
				var testy=cob.y+ctp.y+cob.yy;
				let bx=Math.floor(testx);
				let by=Math.floor(testy);
				if(map.b[bx])
				if(map.b[bx][by])
				{
					let bl=map.b[bx][by];
					if(bl.t!=0)
					{
						if(cob.yy>0)
							cob.y=by-0.45-0.0001;
						cob.yy=0;

					}
				}
			}

			for(let r=0;r<4;r++)
			{
				var ctp=testp[r];

				var testx=cob.x+ctp.x+cob.xx;
				var testy=cob.y+ctp.y+cob.yy;
				let bx=Math.floor(testx);
				let by=Math.floor(testy);
				if(map.b[bx])
				if(map.b[bx][by])
				{
					let bl=map.b[bx][by];
					if(bl.t!=0)
					{
						cob.xx=0;//alert('XX');
					}
				}
			}

			cob.y+=cob.yy;
			cob.x+=cob.xx;
			
		}
		if(cob.t==2)//mobs
		{
			let inf=mobs_info[cob.e_t];


			if(inf.move_type == 1)
			{
				cob.yy+=0.02;
				//cob.xx+=0.02;
				cob.xx*=0.7;


				let xsize=Math.floor(inf.sizex+2);
				let ysize=Math.floor(inf.sizey+2);



				if(cob.keys.a==1)
				{
					cob.xx+=-inf.move_speed;
				}
				if(cob.keys.d==1)
				{
					cob.xx+=inf.move_speed;
				}
				if(cob.keys.w==1)
				{

					for(let r=0;r<4;r++)
					{
						var ctp=testp[r];

						var testx=cob.x+ctp.x;
						var testy=cob.y+ctp.y+0.03;
						let bx=Math.floor(testx);
						let by=Math.floor(testy);
						if(map.b[bx])
						if(map.b[bx][by])
						{
							let bl=map.b[bx][by];
							if(bl.t!=0)
							{
								cob.yy=-0.3;
							}
						}
					}
				}


				if(cob.yy>0.5) cob.yy=0.5;
				if(cob.x< 0.5)cob.x=0.5;
				if(cob.x>map.x-0.5)cob.x=map.x-0.5;
				if(cob.y< 0.5)cob.y=0.5;
				if(cob.y>map.y-0.5)cob.y=map.y-0.5;


				if(cob.yy>0)
				{
					if(!test_poss_mov(map,cob.x,cob.y+cob.yy,inf.sizex,inf.sizey,1))
					{
						let by=Math.floor(cob.y+cob.yy+inf.sizey/2);
						cob.y=by-inf.sizey/2-0.0001;
						cob.yy=0;
					}
				}
				else
				{
					if(!test_poss_mov(map,cob.x,cob.y+cob.yy,inf.sizex,inf.sizey,0))
					{
						let by=Math.floor(cob.y+cob.yy-inf.sizey/2);
						cob.y=by+inf.sizey/2+0.0001;
						cob.yy=0;
					}
				}
				
				if(cob.xx>0)
				{
					if(!test_poss_mov(map,cob.x+cob.xx,cob.y+cob.yy,inf.sizex,inf.sizey,3))
					{
						let bx=Math.floor(cob.x+cob.xx+inf.sizex/2);
						cob.x=bx-inf.sizex/2-0.0001;
						cob.xx=0;
					}
				}
				else
				{
					if(!test_poss_mov(map,cob.x+cob.xx,cob.y+cob.yy,inf.sizex,inf.sizey,2))
					{
						let bx=Math.floor(cob.x+cob.xx-inf.sizex/2);
						cob.x=bx+inf.sizex/2+0.0001;
						cob.xx=0;
					}
				}
				
/*   old version works but uncopyable
				for(let r=0;r<xsize;r++)
				{
					let tx=-inf.sizex/2+r*inf.sizex/(xsize-1);
					let ty=inf.sizey/2;
					if(cob.yy<0)
						ty=-inf.sizey/2;

					var testx=cob.x+tx;
					var testy=cob.y+ty+cob.yy;

					let bx=Math.floor(testx);
					let by=Math.floor(testy);
					if(map.b[bx])
					if(map.b[bx][by])
					{
						let bl=map.b[bx][by];
						if(bl.t!=0)
						{
							if(cob.yy>0)
								cob.y=by-inf.sizey/2-0.0001;
							if(cob.yy<0)
								cob.y=by+inf.sizey/2+0.0001;
							cob.yy=0;
						}
					}
				}



				for(let r=0;r<ysize;r++)
				{
					let tx=inf.sizex/2;
					if(cob.xx<0)
						tx=-inf.sizex/2;
					let ty=-inf.sizey/2+r*inf.sizey/(ysize-1);

					var testx=cob.x+tx+cob.xx;
					var testy=cob.y+ty+cob.yy;

					let bx=Math.floor(testx);
					let by=Math.floor(testy);
					if(map.b[bx])
					if(map.b[bx][by])
					{
						let bl=map.b[bx][by];
						if(bl.t!=0)
						{
							if(cob.xx>0)
								cob.x=bx-inf.sizex/2-0.0001;
							if(cob.xx<0)
								cob.x=bx+inf.sizex/2+0.0001;
							cob.xx=0;
						}
					}
				}
*/

				cob.y+=cob.yy;
				cob.x+=cob.xx;
			}

		}
		if(cob.t==3)//shoots
		{
			
		}


		if(cob.hp<=0)
		{
			
		}

	}

	return cob;
}

/*
move types

1-basic move left right jump


atta

k types
1-body...
*/

var mobs_info=
[
//0
{sizex:1,sizey:1,hp:50,armor:2,move_type:1,move_speed:0.01,move_jump:-0.1,attack_type:1,attack_speed:5,attack_shoot:-1,attack_range:1},
//{sizex:1,sizey:1,hp:50,armor:2,move_type:1,move_speed:0.01,move_jump:-0.1,attack_type:1,attack_speed:5,attack_shoot:-1,attack_range:1},
{sizex:1.8,sizey:0.9,hp:20,armor:2,move_type:1,move_speed:0.01,move_jump:-0.1,attack_type:1,attack_speed:5,attack_shoot:-1,attack_range:1},
{sizex:0.9,sizey:1.8,hp:20,armor:2,move_type:1,move_speed:0.01,move_jump:-0.1,attack_type:1,attack_speed:5,attack_shoot:-1,attack_range:1}
]

function upd_obj(obj,map)
{
	for(let key in obj)
	{
		obj[key]=upd_single_obj(obj[key],map,1);

		if(obj[key].t==0)
			delete obj[key];
	}
}



try
{
	module.exports.test_poss_mov=test_poss_mov;

	module.exports.upd=upd_obj;
	module.exports.mobs_info=mobs_info;
	module.exports.add_obj=add_obj;
}catch(e){}
















