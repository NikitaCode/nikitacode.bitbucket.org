var events = require('events');
 
module.exports = function() {
  var pubsub = new events.EventEmitter();
  pubsub.clients = {};
  pubsub.subscriptions = {};
 
  pubsub.on('join', function(socket) {
    socket['_id'] = socket.remoteAddress + ':' + socket.remotePort;    
    this.clients[socket['_id']] = socket;
 
    this.subscriptions[socket['_id']] = function(client, data) {
      if (socket['_id'] != client['_id']) {
        this.clients[socket['_id']].write(data);
      }      
    }
    this.on('broadcast', this.subscriptions[socket['_id']]);
 
    console.log('--- socket saved ---\nusers online: %d', this.listeners('broadcast').length);
    socket.write('Welcome to Chamber of Secrets!');
  });
 
  pubsub.on('leave', function(socket) {
    delete pubsub.clients[socket['_id']];
    this.removeListener('broadcast', this.subscriptions[socket['_id']]);    
    socket.destroy();
    console.log('--- socket destroyed ---\nusers online: %d', this.listeners('broadcast').length);
  });
 
  pubsub.on('error', function(e) {
    console.log('--- pubsub error ---\n%s', e.message);
  });
 
  return pubsub;
}