


/*

server protocol
{'event': 'message','id':?,'text':?}
{'event': 'setpos','id':?,'pos':{x:?,y:?}}


*/


var ClassicalNoise = function(r) { // Classic Perlin noise in 3D, for comparison 
	if (r == undefined) r = Math;
  this.grad3 = [[1,1,0],[-1,1,0],[1,-1,0],[-1,-1,0], 
                                 [1,0,1],[-1,0,1],[1,0,-1],[-1,0,-1], 
                                 [0,1,1],[0,-1,1],[0,1,-1],[0,-1,-1]]; 
  this.p = [];
  for (var i=0; i<256; i++) {
	  this.p[i] = Math.floor(r.random()*256);
  }
  // To remove the need for index wrapping, double the permutation table length 
  this.perm = []; 
  for(var i=0; i<512; i++) {
		this.perm[i]=this.p[i & 255];
  }
};

ClassicalNoise.prototype.dot = function(g, x, y, z) { 
    return g[0]*x + g[1]*y + g[2]*z; 
};

ClassicalNoise.prototype.mix = function(a, b, t) { 
    return (1.0-t)*a + t*b; 
};

ClassicalNoise.prototype.fade = function(t) { 
    return t*t*t*(t*(t*6.0-15.0)+10.0); 
};

  // Classic Perlin noise, 3D version 
ClassicalNoise.prototype.noise = function(x, y, z) { 
  // Find unit grid cell containing point 
  var X = Math.floor(x); 
  var Y = Math.floor(y); 
  var Z = Math.floor(z); 
  
  // Get relative xyz coordinates of point within that cell 
  x = x - X; 
  y = y - Y; 
  z = z - Z; 
  
  // Wrap the integer cells at 255 (smaller integer period can be introduced here) 
  X = X & 255; 
  Y = Y & 255; 
  Z = Z & 255;
  
  // Calculate a set of eight hashed gradient indices 
  var gi000 = this.perm[X+this.perm[Y+this.perm[Z]]] % 12; 
  var gi001 = this.perm[X+this.perm[Y+this.perm[Z+1]]] % 12; 
  var gi010 = this.perm[X+this.perm[Y+1+this.perm[Z]]] % 12; 
  var gi011 = this.perm[X+this.perm[Y+1+this.perm[Z+1]]] % 12; 
  var gi100 = this.perm[X+1+this.perm[Y+this.perm[Z]]] % 12; 
  var gi101 = this.perm[X+1+this.perm[Y+this.perm[Z+1]]] % 12; 
  var gi110 = this.perm[X+1+this.perm[Y+1+this.perm[Z]]] % 12; 
  var gi111 = this.perm[X+1+this.perm[Y+1+this.perm[Z+1]]] % 12; 
  
  // The gradients of each corner are now: 
  // g000 = grad3[gi000]; 
  // g001 = grad3[gi001]; 
  // g010 = grad3[gi010]; 
  // g011 = grad3[gi011]; 
  // g100 = grad3[gi100]; 
  // g101 = grad3[gi101]; 
  // g110 = grad3[gi110]; 
  // g111 = grad3[gi111]; 
  // Calculate noise contributions from each of the eight corners 
  var n000= this.dot(this.grad3[gi000], x, y, z); 
  var n100= this.dot(this.grad3[gi100], x-1, y, z); 
  var n010= this.dot(this.grad3[gi010], x, y-1, z); 
  var n110= this.dot(this.grad3[gi110], x-1, y-1, z); 
  var n001= this.dot(this.grad3[gi001], x, y, z-1); 
  var n101= this.dot(this.grad3[gi101], x-1, y, z-1); 
  var n011= this.dot(this.grad3[gi011], x, y-1, z-1); 
  var n111= this.dot(this.grad3[gi111], x-1, y-1, z-1); 
  // Compute the fade curve value for each of x, y, z 
  var u = this.fade(x); 
  var v = this.fade(y); 
  var w = this.fade(z); 
   // Interpolate along x the contributions from each of the corners 
  var nx00 = this.mix(n000, n100, u); 
  var nx01 = this.mix(n001, n101, u); 
  var nx10 = this.mix(n010, n110, u); 
  var nx11 = this.mix(n011, n111, u); 
  // Interpolate the four results along y 
  var nxy0 = this.mix(nx00, nx10, v); 
  var nxy1 = this.mix(nx01, nx11, v); 
  // Interpolate the two last results along z 
  var nxyz = this.mix(nxy0, nxy1, w); 

  return nxyz; 
};

var no=new ClassicalNoise;



var map={t:0,x:0,y:0,b:[]}

block_textures=new Image(); 
block_textures.src = 'icons.png';

function init_map(_x,_y)
{
	map.chunk=[];
	map.b=[];
	for(let i=0;i<_x;i++)
	{
		map.b[i]=[];
		for(let r=0;r<_y;r++)
		{
			map.b[i][r]={t:-1};
		}
	}
	for(let i=0;i<_x/4;i++)
	{
		map.chunk[i]=[];
		for(let r=0;r<_y/4;r++)
		{
			map.chunk[i][r]={t:-1,time:0};
		}
	}
	map.x=_x;
	map.y=_y;
}
function gen_map()
{
	for(let i=0;i<map.x;i++)
	{
		map.b[i]=[];
		for(let r=0;r<map.y;r++)
		{
			map.b[i][r]={t:0};
			var o=Math.floor(no.noise(3,3,i/10)*7)+map.y/2;

			if(r==o)
				map.b[i][r].t=1;
			if(r>o)
				map.b[i][r].t=2;
		}
	}
}

var player={x:1,y:1,camx:5,camy:390,id:null};

var myplayerid=-1;

var cid = 'g85n73u86';
var cx = document.getElementById(cid).getContext("2d");

let razoky=document.getElementById(cid).height;
let razokx=document.getElementById(cid).width;

var tick=0;

var obj={};
/*
t:
0-????
1-player
2-item
3-enemy
4-bullet

*/



function drawblock(_x,_y,_b)
{
	try
	{
		cx.drawImage(block_textures,0+_b.t*32,0,32,32,_x,_y,32,32);
	}
	catch(e){}
}

function circlehalf(_x,_y,_xx,_rp)
{

	cx.beginPath();
	cx.arc(_x, _y+_xx/2-_xx/2*_rp, _xx/2, -1* Math.PI, 0 );
	cx.arc(_x+_xx/2-_xx/2*_rp, _y+_xx/2-_xx/2*_rp, _xx/2*_rp, 0 * Math.PI, 0.5 * Math.PI);
	//cx.moveTo(graph.point[selected_point].x,graph.point[selected_point].y);
	//cx.lineTo(cursor.x,cursor.y);
	cx.arc(_x-_xx/2+_xx/2*_rp, _y+_xx/2-_xx/2*_rp, _xx/2*_rp, 0.5 * Math.PI, 1 * Math.PI);
	cx.stroke();	
}

function drawplayer(_pla)
{
	cx.strokeStyle = "#AAAAAA";
	circlehalf((_pla.x-player.camx)*32,(_pla.y-player.camy)*32,10,1);
	//if (Math.abs(_pla.yy)<0.001)
	//	circlehalf((_pla.x-player.camx)*32,(_pla.y-player.camy)*32,32,0.5);
	//else
	//	circlehalf((_pla.x-player.camx)*32,(_pla.y-player.camy)*32,32,0.9);
}

function drawobj(cob)
{
	cx.strokeStyle = "#000000";
	if(cob.t==1)//player
	{
		if (Math.abs(cob.yy)==0)
			circlehalf((cob.x-player.camx)*32,(cob.y-player.camy)*32,32,0.5);
		else
			circlehalf((cob.x-player.camx)*32,(cob.y-player.camy)*32,32,0.9);
	}
	// this mob..... this rectangle
	if(cob.t==2)//mob
	{
		cx.beginPath();
		cx.rect((cob.x-player.camx-mobs_info[cob.e_t].sizex/2)*32,(cob.y-player.camy-mobs_info[cob.e_t].sizey/2)*32,mobs_info[cob.e_t].sizex*32,mobs_info[cob.e_t].sizey*32);
		cx.stroke();
	}
}

function draw()
{
	cx.clearRect(0,0,razokx,razoky);
	let stx=Math.floor(player.camx)-1;
	let sty=Math.floor(player.camy)-1;
	let endx=Math.floor(player.camx)+razokx/32+1;
	let endy=Math.floor(player.camy)+razoky/32+1;

	if(stx<0)stx=0;
	if(stx>=map.x)stx=map.x-1;
	if(sty<0)sty=0;
	if(sty>=map.y)sty=map.y-1;

	if(endx>map.x)endx=map.x;

	if(endy>map.y)endy=map.y;


	for(let i=stx;i<endx;i++)

	{
		for(let r=sty;r<endy;r++)
		{
			if(i*32-Math.floor(player.camx*32)<razokx)
			if(i*32-Math.floor(player.camx*32)>-32)
			if(r*32-Math.floor(player.camy*32)<razoky)
			if(r*32-Math.floor(player.camy*32)>-32)
			drawblock(i*32-Math.floor(player.camx*32),r*32-Math.floor(player.camy*32),map.b[i][r]);

		}
	}

	for(let key in obj)
	{
		let cob=obj[key];
	
		drawobj(cob);
	}

	for(let i=0;i<players.length;i++)
	if(players[i]!=null)
	{

		drawplayer(players[i]);
		//cx.drawImage(block_textures,0+3*32,0,32*0.9,32*0.9,players[i].x*32-32*0.9/2-player.camx*32,players[i].y*32-32*0.9/2-player.camy*32,32,32);
	}

}


function upd()
{
	let my_obj=-1;
	if(players[player.id])
		my_obj=players[player.id].id_obj;

	ti++;
/*
	if(players.length==0)
		return;
	if(myplayerid==-1)
		return;

	player.camx=players[myplayerid].x-razokx/2/32;
	player.camy=players[myplayerid].y-razoky/2/32;

	if(map.b[Math.floor(players[myplayerid].x)])
	if(map.b[Math.floor(players[myplayerid].x)][Math.floor(players[myplayerid].y)])
	if(map.b[Math.floor(players[myplayerid].x)][Math.floor(players[myplayerid].y)].t==-1)
		return;
*/

	if(obj[my_obj] != null)
	{
		player.camx=obj[my_obj].x-razokx/2/32;
		player.camy=obj[my_obj].y-razoky/2/32;
		player.x=obj[my_obj].x;
		player.y=obj[my_obj].y;
		players[player.id].x=obj[my_obj].x;
		players[player.id].y=obj[my_obj].y;
		obj[my_obj].keys.w=keys['W'.charCodeAt(0)];
		obj[my_obj].keys.s=keys['S'.charCodeAt(0)];
		obj[my_obj].keys.a=keys['A'.charCodeAt(0)];
		obj[my_obj].keys.d=keys['D'.charCodeAt(0)];
	}


	upd_obj(obj,map);

	for(let i=0;i<players.length;i++)
	if(players[i]!=null){
		//players[i].x+=Math.random()*10-Math.random()*10;
		//players[i].y+=Math.random()*10-Math.random()*10;


		//io.sockets.json.send({'event': 'players', 'text': players});
			//alert('Прошла 1 секунда');
	}

}

init_map(800,800);


/*
let d=new ArrayBuffer(8);
var uint8s = new Uint8Array(d);

for (let i = 0; i < 8; i++) {
    uint8s[i] = 5; // fill each byte with 0x05
}
alert(JSON.stringify({d:d}));//not work... {}
alert(JSON.stringify({d:uint8s}));//so bad {"0":5,"1":5,"2":5,...}
alert(JSON.stringify({d:ab2str(d)}));//work but... "new"
alert('....');
*/


function apply_part(_part)
{
	map.chunk[_part.x/4][_part.y/4].t=1;			
	for(let i=0;i<4;i++)
	for(let r=0;r<4;r++)
	{
		map.b[i+_part.x][r+_part.y]=_part[i][r];
	}
}

function serv_synch()
{
	for(let i=0;i<map.x/4;i++)
	{
		for(let r=0;r<map.y/4;r++)
		{
			map.chunk[i][r].time=Math.max(0,map.chunk[i][r].time-1);			
		}
	}
	let chunkposx=Math.floor(player.x/4);
	let chunkposy=Math.floor(player.y/4);

	for(let i=-4+chunkposx;i<=4+chunkposx;i++)
	for(let r=-4+chunkposy;r<=4+chunkposy;r++)
	{
		if(i>=0 && r>=0 && i<map.x/4 && r<map.y/4)
		if(map.chunk[i][r].t==-1 && map.chunk[i][r].time==0)
		{
			socket.send({'event': 'need_part', x:i*4,y:r*4});
			map.chunk[i][r].time=40;			
		}

	}

	for(let key in obj)
	{
		if(obj[key].last_ti)
		if(ti-obj[key].last_ti>200)
		{
			delete obj[key];
		}
	}
	socket.send({'event': 'player_info',player:players[player.id]});

	let my_obj=-1;
	if(players[player.id])
		my_obj=players[player.id].id_obj;
	if(obj[my_obj] != null)
	{
		socket.send({'event': 'obj_info', 'obj': obj[my_obj]});
	}

}

var color_num = [
"#000000",
"#FF0000",
"#00FF00",
"#0000FF",
"#FFFF00",
"#FF00FF",
"#00FFFF"
];


var last_t=new Date();
var times=[0,0];
var timesm=[0,0];
var ti=0;
var sv_ti=0;


times.length=100;

window.onload = function() {
	// Создаем соединение с сервером; websockets почему-то в Хроме не работают, используем xhr
	if (navigator.userAgent.toLowerCase().indexOf('chrome') != -1) {
		//socket = io.connect('http://109.172.10.205:8080', {'transports': ['xhr-polling']});
		//socket = io.connect('http://109.172.10.205:8080');
	} else {
		//socket = io.connect('http://109.172.10.205:8080');
	}
	//socket = io.connect('http://127.0.0.1:8080');
	socket = io.connect('http://109.172.10.205:8080');
	function process_upd() 
	{
		//last_t=new Date();

		let t1 = new Date();
		times.splice(0,0,t1-last_t);
		//alert((t1-last_t)+"  "+times);
		times.length=500;
		last_t=t1;

		if(sv_ti-ti>-2)
			upd(30);

		sv_ti++;
		upd(30);
		serv_synch();
		draw();


		//cx.clearRect(0,0,razokx,razoky);
		for(let i=0;i<times.length-1;i++)
		{

			cx.beginPath();
			cx.moveTo(i,500-times[i]);
			cx.lineTo(i+1,500-times[(i+1)]);
			cx.stroke();

			cx.beginPath();
			cx.moveTo(i,500-timesm[i]);
			cx.lineTo(i+1,500-timesm[i+1]);
			cx.stroke();	
		}



		let t2 = new Date();
		timesm.splice(0,0,t2-t1);
		//alert((t1-last_t)+"  "+times);
		timesm.length=500;
		//document.querySelector('#log').innerHTML+=' '+JSON.stringify(t2-t1);
		//last_t=t2;
		//document.querySelector('#log').innerHTML=JSON.stringify(players)+" <br> "+JSON.stringify(player);

		document.querySelector('#log').innerHTML= sv_ti-ti+'<br>'+JSON.stringify(obj);

		setTimeout(process_upd,Math.max(30-(t2-t1),5));
	}
	process_upd();

	socket.on('connect', function () {
	
		socket.on('message', function (msg) {

		//last_t=new Date();

			//document.querySelector('#log').innerHTML += 'M:'+JSON.stringify(msg) + '<br>';
			// Добавляем в лог сообщение, заменив время, имя и текст на полученные
			
			//document.querySelector('#log').innerHTML += JSON.stringify(msg) + '<br>';
			if(document.querySelector('#log').innerHTML.length>10000)
				document.querySelector('#log').innerHTML=0;

			if(msg.event=='userSplit')
			{
				document.querySelector('#log').innerHTML += 'Player with ID:'+ msg.id + ' disconnected.<br>';
			}
			if(msg.event=='userJoined')
			{
				document.querySelector('#log').innerHTML += 'Player joined hes ID:'+ msg.id + '.<br>';
			}
			if(msg.event=='y_obj')
			{
				
				//alert(JSON.stringify(players)+"  <br>"+JSON.stringify(player));
			}
			if(msg.event=='obj_info')
			{
				if(!players[player.id] || msg.id!=players[player.id].id_obj)
				{
					obj[msg.id]=upd_single_obj(msg.obj,map,ti-msg.tick);

					obj[msg.id].last_ti=ti;
				}
				else
				{
					if(!obj[msg.id])
						obj[msg.id]=msg.obj;
				}
				//alert(JSON.stringify(msg.obj));
			}
			if(msg.event=='connected')
			{
				//document.querySelector('#log').innerHTML += 'You connected. Your ID:'+ msg.id + '.<br>';
				player.id=msg.id;
				players=msg.players;
				ti=msg.tick;
				//alert(JSON.stringify(players)+"  <br>"+JSON.stringify(player));
			}
			if(msg.event=='messageSent')
			{
				document.querySelector('#log').innerHTML += 'You:'+msg.text + '<br>';
			}
			if(msg.event=='messageReceived')
			{
				document.querySelector('#log').innerHTML += msg.id+":" + msg.text + '<br>';
			}

			if(msg.event=='map_part')
			{
				apply_part(msg.part);
			}


			if(msg.event=='players')
			{
				//document.querySelector('#log').innerHTML='play:'+JSON.stringify(players);
				msg.text[myplayerid]=players[myplayerid];
				players = msg.text;
				sv_ti=msg.tick;



				//alert(this.cid);
				//return;

				//PROC

/*

				let razoky=document.getElementById(cid).height;
				let razokx=document.getElementById(cid).width;

				cx.clearRect(0,0,razokx,razoky);


				for(let i=0;i<players.length;i++)
				if(players[i]!=null){
					cx.beginPath();
					cx.arc(players[i].x, players[i].y, 10, 0, 2 * Math.PI);
					cx.closePath();
					cx.fillStyle = color_num[i];
					cx.fill();
				}
				*/

			}

			//document.querySelector('#log').innerHTML += strings[msg.event].replace(/\[([a-z]+)\]/g, '<span class="$1">').replace(/\[\/[a-z]+\]/g, '</span>').replace(/\%time\%/, msg.time).replace(/\%name\%/, msg.name).replace(/\%text\%/, unescape(msg.text).replace('<', '&lt;').replace('>', '&gt;')) + '<br>';
			
		//let t2 = new Date();
		//document.querySelector('#log').innerHTML+=' '+JSON.stringify(t2-last_t);
			// Прокручиваем лог в конец
			//document.querySelector('#log').scrollTop = document.querySelector('#log').scrollHeight;
		});
		// При нажатии <Enter> или кнопки отправляем текст
		document.querySelector('#input').onkeypress = function(e) {
			if (e.which == '13') {
				// Отправляем содержимое input'а, закодированное в escape-последовательность
			socket.send({event: 'message',id:'0',text:document.querySelector('#input').value});
				// Очищаем input
				document.querySelector('#input').value = '';
			}
		};
		document.querySelector('#send').onclick = function() {
			//alert(JSON.stringify(escape(document.querySelector('#input').value)));
			socket.send({event: 'message',id:'0',text:document.querySelector('#input').value});
			document.querySelector('#input').value = '';
		};		
	});
};



var players=[];





document.getElementById(cid).onmousedown = on_m_down;

function on_m_down(e)
{

	let cv = document.getElementById('g85n73u86');
	let cx = document.getElementById('g85n73u86').getContext("2d");


	let x=e.pageX-cv.offsetLeft;
	let y=e.pageY-cv.offsetTop;

	players[myplayerid].x=(x+player.camx*32)/32.0;
	players[myplayerid].y=(y+player.camy*32)/32.0;

	//socket.send({event: 'setpos',id:'0',pos:{x:x,y:y}});


	//upd.call(array_of_models[arrid]);
}





document.getElementById('g85n73u86').addEventListener('keydown', handle, true);
document.getElementById('g85n73u86').addEventListener('keyup', handle1, true);
		
var keys=[];	


	function handle(e) 
	{
		//if (form.elements[e.type + 'Ignore'].checked) return;

		if(e.type=='keydown')
		{
			var ch=String.fromCharCode(e.keyCode);
			if (ch>='A' && ch<='Z' || ch>='0' && ch<='9')
			{

				keys[ch.charCodeAt(0)]=1;

				//alert(JSON.stringify(array_of_models[arrid].keys));
				//upd.call(array_of_models[arrid]);
			}
		}
	}
	function handle1(e) 
	{
		//if (form.elements[e.type + 'Ignore'].checked) return;

		if(e.type=='keyup')
		{
			var ch=String.fromCharCode(e.keyCode);
			if (ch>='A' && ch<='Z' || ch>='0' && ch<='9')
			{

				keys[ch.charCodeAt(0)]=0;//alert('xxx');

				//alert(JSON.stringify(array_of_models[arrid].keys));
				//upd.call(array_of_models[arrid]);
			}
		}
	}




